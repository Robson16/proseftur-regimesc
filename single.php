<?php

/**
 * The template for displaying all single posts
 *
 */
get_header();
?>
<main>
    <div class="container pt-5">
        <?php
        while (have_posts()) {
            the_post();
            get_template_part( 'partials/content/content' );
            if ( comments_open() || get_comments_number() ) comments_template(); 
        }
        ?>
    </div>
    <!-- /.container -->
</main>

<?php
get_footer();
