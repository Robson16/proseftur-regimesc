<?php

/** 
 * Theme configuration
 */
function proseftur_regimesc_config()
{
    // Habilitando suporte à tradução
    $textdomain = "proseftur-regimesc";
    load_theme_textdomain($textdomain, get_stylesheet_directory() . "/languages/");
    load_theme_textdomain($textdomain, get_template_directory() . "/languages/");

    // Menu registration
    register_nav_menus(array(
        "main_menu" => __("Main Menu", "proseftur-regimesc"),
    ));

    // Adding theme supports
    add_theme_support("title-tag");
    add_theme_support("custom-logo", array(
        "height" => 153,
        "width" => 345,
        "flex-height" => true,
        "flex-width" => true,
        "header-text" => array("site-title", "site-description"),
    ));
    add_theme_support("post-thumbnails");
    add_theme_support("responsive-embeds");
    add_theme_support("align-wide");
}

add_action("after_setup_theme", "proseftur_regimesc_config");

/** 
 * Loading Scripts and Style Sheets
 */
function proseftur_regimesc_scripts()
{
    // CSS
    wp_enqueue_style("bootstrap", "//stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css", array(), "4.4.1", "all");
    wp_enqueue_style("proseftur-regimesc-webfont", get_template_directory_uri() . "/assets/css/webfonts.css", array(), "1.0", "all");
    wp_enqueue_style("proseftur-regimesc-style", get_template_directory_uri() . "/assets/css/style.css", array("bootstrap"), wp_get_theme()->get("Version"));

    // Js
    wp_deregister_script("jquery");
    wp_register_script("jquery", "//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js", false, "3.4.1");
    wp_enqueue_script("jquery");
    wp_enqueue_script("popper", "//cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js", array("jquery"), "4.4.1", true);
    wp_enqueue_script("bootstrap", "//stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js", array("jquery", "popper"), "4.4.1", true);
    wp_enqueue_script("main", get_template_directory_uri() . "/assets/js/main.js", array("jquery"), "1.0", true);
}

add_action("wp_enqueue_scripts", "proseftur_regimesc_scripts");

/**
 * Registration of widget areas.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function proseftur_regimesc_sidebars()
{

    // Arguments used in all register_sidebar() calls.
    $shared_args = array(
        "before_title" => "<h4 class='widget-title'>",
        "after_title" => "</h4>",
        "before_widget" => "<div class='widget %2$s'><div class='widget-content'>",
        "after_widget" => "</div></div>",
    );

    // Footer #1.
    register_sidebar(array_merge($shared_args, array(
        "name" => __("Footer #1", "proseftur-regimesc"),
        "id" => "proseftur-regimesc-sidebar-footer-1",
        "description" => __("The widgets in this area will be displayed in the first column in the footer.", "proseftur-regimesc"),
    )));

    // Footer #2.
    register_sidebar(array_merge($shared_args, array(
        "name" => __("Footer #2", "proseftur-regimesc"),
        "id" => "proseftur-regimesc-sidebar-footer-2",
        "description" => __("The widgets in this area will be displayed in the second column in the footer.", "proseftur-regimesc"),
    )));

    // Footer #3.
    register_sidebar(array_merge($shared_args, array(
        "name" => __("Footer #3", "proseftur-regimesc"),
        "id" => "proseftur-regimesc-sidebar-footer-3",
        "description" => __("The widgets in this area will be displayed in the third column in the footer.", "proseftur-regimesc"),
    )));

    //Footer #4.
    register_sidebar(array_merge($shared_args, array(
        "name" => __("Footer #4", "proseftur-regimesc"),
        "id" => "proseftur-regimesc-sidebar-footer-4",
        "description" => __("The widgets in this area will be displayed in the fourth column in the footer.", "proseftur-regimesc"),
    )));
}

add_action("widgets_init", "proseftur_regimesc_sidebars");

/**
 *  WordPress Bootstrap Nav Walker
 */
require_once get_template_directory() . "/includes/classes/class-wp-bootstrap-navwalker.php";

/**
 *  WordPress Bootstrap Pagination
 */
require_once get_template_directory() . "/includes/wp-bootstrap4.1-pagination.php";

/**
 *  TGM Plugin Activation
 */
require_once get_template_directory() . "/includes/required-plugins.php";

/**
 * Extra functions, filters, and actions for the theme
 */
require get_template_directory() . "/includes/template-functions.php";
