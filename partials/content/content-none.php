<?php
/*
 * Template Part para exibir que nenhuma publicação foi encontrada
 */
?>

<div class="col">
    <h3 class="text-center"><?php _e('No content to display', 'proseftur-regimesc'); ?></h3>
</div>
