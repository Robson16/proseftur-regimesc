<?php
if (has_post_thumbnail()) {
    the_post_thumbnail( 'medium', array(
        'class' => 'img-fluid',
        'title' => get_the_title(),
        'alt' => get_the_title(),
    ) );
}
?>

<header class="entry-header">
    <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
</header><!-- .entry-header -->

<div class="entry-content">
    <?php the_content(); ?>
</div><!-- .entry-content -->