</main>

<footer id="footer" class="bg-color-six text-white">
    <div class="container py-5">
        <div class="row ">
            <div class="col-12 col-md-6 col-lg-3">
                <?php if ( is_active_sidebar( 'proseftur-regimesc-sidebar-footer-1' ) ) dynamic_sidebar( 'proseftur-regimesc-sidebar-footer-1' ); ?>
            </div>
            <!-- /.col -->
            <div class="col-12 col-md-6 col-lg-3">
                <?php if ( is_active_sidebar( 'proseftur-regimesc-sidebar-footer-2' ) ) dynamic_sidebar( 'proseftur-regimesc-sidebar-footer-2' ); ?>
            </div>
            <!-- /.col -->
            <div class="col-12 col-md-6 col-lg-3">
                <?php if ( is_active_sidebar( 'proseftur-regimesc-sidebar-footer-3' ) ) dynamic_sidebar( 'proseftur-regimesc-sidebar-footer-3' ); ?>
            </div>
            <!-- /.col -->
            <div class="col-12 col-md-6 col-lg-3">
                <?php if ( is_active_sidebar( 'proseftur-regimesc-sidebar-footer-4' ) ) dynamic_sidebar( 'proseftur-regimesc-sidebar-footer-4' ); ?>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>

    <div class="copyright container py-3">
        <div class="row text-white">
            <div class="col-12 col-md-6">
                <p class=" text-center text-md-left m-0">
                    &#9400; <?php echo date('Y') . ' ' . get_bloginfo('name') . ' - ' . __( 'All rights reserved.', 'proseftur-regimesc' ); ?>
                </p>
            </div>
            <!-- /.col -->
            <div class="col-12 col-md-6">
                <p class="text-center text-md-right m-0">
                    <?php _e( 'Design by', 'proseftur-regimesc' ); ?> <a href="http://novahorda.com/" target="_blank" rel="noopener">NHRD</a>
                </p>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</footer>

<?php wp_footer(); ?>

</body>

</html>